#include <iomanip>
#include <strings.h>
#include <netinet/in.h>
#include "xsens/protocol.hpp"

namespace xsens
{

//-----------------------------------------------------------------------------
inline uint16_t to_uint16(const uint8_t *data)
{
    return htons(*(uint16_t *)data);
}

//-----------------------------------------------------------------------------
inline uint32_t to_uint32(const uint8_t *data)
{
    return htonl(*(uint32_t *)data);
}

//-----------------------------------------------------------------------------
inline float to_float32(const uint8_t *data)
{
    union {
        uint32_t data;
        float value;
    } tmp;

    tmp.data =  htonl(*(uint32_t *)data);
    return tmp.value;
}

//-----------------------------------------------------------------------------
void parse_frame(const uint8_t *buffer, int len, data_t &data)
{
    int i=0;
    bzero((void*)&data, sizeof(data));

    while(i<len)
    {
        uint16_t id = (buffer[i]<<8)+buffer[i+1];
        uint8_t len = buffer[i+2];

        switch(id)
        {
        case xdi::PacketCounter:
            data.packet_counter = to_uint16(&buffer[i+3]);
            data.packet_counter_flag = true;
            break;

        case xdi::SampleTimeFine:
            data.sample_time = to_uint32(&buffer[i+3]);
            data.sample_time_flag = true;
            break;

        case xdi::StatusByte:
            data.status_byte = buffer[i+3];
            break;

        case xdi::Quaternion:
            data.orientation.w = to_float32(&buffer[i + 3]);
            data.orientation.x = to_float32(&buffer[i + 3 + 4]);
            data.orientation.y = to_float32(&buffer[i + 3 + 8]);
            data.orientation.z = to_float32(&buffer[i + 3 + 12]);
            data.orientation_flag = true;
            break;

        case xdi::Acceleration:
            data.linear_acceleration.x = to_float32(&buffer[i + 3]);
            data.linear_acceleration.y = to_float32(&buffer[i + 3 + 4]);
            data.linear_acceleration.z = to_float32(&buffer[i + 3 + 8]);
            data.linear_acceleration_flag = true;
            break;

        case xdi::RateOfTurn:
            data.angular_velocity.x = to_float32(&buffer[i + 3]);
            data.angular_velocity.y = to_float32(&buffer[i + 3 + 4]);
            data.angular_velocity.z = to_float32(&buffer[i + 3 + 8]);
            data.angular_velocity_flag = true;
            break;

        case xdi::MagneticField:
            data.magnetic_field.x = to_float32(&buffer[i + 3]);
            data.magnetic_field.y = to_float32(&buffer[i + 3 + 4]);
            data.magnetic_field.z = to_float32(&buffer[i + 3 + 8]);
            data.magnetic_field_flag=true;
            break;

        case xdi::DeltaQ:
            data.delta_q.w = to_float32(&buffer[i + 3]);
            data.delta_q.x = to_float32(&buffer[i + 3 + 4]);
            data.delta_q.y = to_float32(&buffer[i + 3 + 8]);
            data.delta_q.z = to_float32(&buffer[i + 3 + 12]);
            data.delta_q_flag=true;
        break;

        case xdi::DeltaV:
            data.delta_v.x = to_float32(&buffer[i + 3]);
            data.delta_v.y = to_float32(&buffer[i + 3 + 4]);
            data.delta_v.z = to_float32(&buffer[i + 3 + 8]);
            data.delta_v_flag=true;
            break;
        }
        i += (len + 3);
    }
}

} // namespace xsens


std::ostream &operator<<(std::ostream &o, const xsens::data_t &data)
{
    if (data.packet_counter_flag)
        o << "Packet counter: " << std::dec << data.packet_counter << std::endl;

    if (data.sample_time_flag)
        o << "Sample time...: " << data.sample_time << std::endl;

    o << "Status Byte...: 0x" << std::hex << int(data.status_byte) << std::endl;


    o << std::fixed << std::setprecision(6);
    if (data.orientation_flag)
        o << "Orientation (w, x, y, z).....: ("
          << data.orientation.w << ", "
          << data.orientation.x << ", "
          << data.orientation.y << ", "
          << data.orientation.z << ")" << std::endl;

    if (data.linear_acceleration_flag)
        o << "Linear acceleration (x, y, z): ("
          << data.linear_acceleration.x << ", "
          << data.linear_acceleration.y << ", "
          << data.linear_acceleration.z << ")" << std::endl;

    if (data.angular_velocity_flag)
        o << "Angular Rotation (x, y, z)...: ("
          << data.angular_velocity.x << ", "
          << data.angular_velocity.y << ", "
          << data.angular_velocity.z << ")" << std::endl;

    if (data.magnetic_field_flag)
        o << "Magnetic field (x, y, z).....: ("
          << data.magnetic_field.x << ", "
          << data.magnetic_field.y << ", "
          << data.magnetic_field.z << ")" << std::endl;

    if (data.delta_q_flag)
        o << "Delta_Q (w, x, y, z).........: ("
          << data.delta_q.w << ", "
          << data.delta_q.x << ", "
          << data.delta_q.y << ", "
          << data.delta_q.z << ")" << std::endl;

    if (data.delta_v_flag)
        o << "Delta_V (x, y, z)............: ("
          << data.delta_v.x << ", "
          << data.delta_v.y << ", "
          << data.delta_v.z << ")" << std::endl;


    return o;
}
