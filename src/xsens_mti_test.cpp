#include <csignal>
#include <thread>
#include <chrono>
#include <iostream>
#include "xsens/serial.hpp"
#include "xsens/protocol.hpp"

using namespace std;

//-----------------------------------------------------------------------------
bool quit = false;
void signalHandler( int signum )
{
    quit = true;
}


//-----------------------------------------------------------------------------
int main()
{
    try
    {
        auto start = chrono::steady_clock::now();
        xsens::Serial xsens_serial;

        // Baudrate : 115200
        // Timeout : 500ms
        xsens_serial.open_device("/dev/ttyUSB0", 115200, 500);

        signal(SIGINT, signalHandler);
        std::cout << "Try to read frames" << std::endl;
        xsens::frame_t frame;
        while (!quit)
        {
            if (xsens_serial.read_frame(frame))
            {
                auto now = chrono::steady_clock::now();
                float dt = chrono::duration_cast<chrono::microseconds>(now - start).count();
                std::cout << "[" << dt << " us] new frame: m_id=" << int(frame.m_id) << std::endl;

                if (frame.m_id == 0x36)
                {
                    xsens::data_t data;
                    xsens::parse_frame(frame.data, frame.len, data);
                    std::cout << data << std::endl;
                    start = now;
                }
            }
        }
    }
    catch(std::string& e)
    {
        std::cout << "Error: " << e << std::endl;
    }

    return 0;
}