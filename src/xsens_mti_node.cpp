#include <csignal>
#include <thread>
#include <chrono>
#include <iostream>
#include "xsens/serial.hpp"
#include "xsens/protocol.hpp"
#include "rclcpp/rclcpp.hpp"
#include "rclcpp/clock.hpp"
#include "geometry_msgs/msg/quaternion_stamped.hpp"
#include "geometry_msgs/msg/vector3_stamped.hpp"
#include "sensor_msgs/msg/imu.hpp"
#include "sensor_msgs/msg/magnetic_field.hpp"

using namespace std;

class XsensMtiNode : public rclcpp::Node
{
  public:
    XsensMtiNode()
        : Node("xsens_mti")
    {
        // Get ROS PARAM
        std::string device;
        int baudrate;
        std::string frame_id;

        this->declare_parameter<std::string>("device", "/dev/ttyUSB0");
        this->declare_parameter<int>("baudrate", 921600);
        this->declare_parameter<std::string>("frame_id", "xsens");

        this->declare_parameter<double>("covariance/orientation/roll", 0.01745);   // From XSens datasheet
        this->declare_parameter<double>("covariance/orientation/pitch", 0.01745);  // 
        this->declare_parameter<double>("covariance/orientation/yaw", 0.15708);    // 

        this->declare_parameter<double>("covariance/angular_velocity/x", 1e-4);    //
        this->declare_parameter<double>("covariance/angular_velocity/y", 1e-4);    //
        this->declare_parameter<double>("covariance/angular_velocity/z", 1e-4);    //

        this->declare_parameter<double>("covariance/linear_acceleration/x", 0.05);  //
        this->declare_parameter<double>("covariance/linear_acceleration/y", 0.05);  //
        this->declare_parameter<double>("covariance/linear_acceleration/z", 0.05);  //



        this->get_parameter("device", device);
        this->get_parameter("baudrate", baudrate);
        this->get_parameter("frame_id", frame_id);

        RCLCPP_INFO(this->get_logger(), "Init node");
        RCLCPP_INFO(this->get_logger(), "Device %s at %i baudrate", device.c_str(), baudrate);

        serial_.open_device(device, baudrate, 20);  // timeout = 20 ms
        serial_.go_to_measurement();

        // Init publisher
        pub_imu_ = this->create_publisher<sensor_msgs::msg::Imu>("~/imu_data", 10);
        pub_mag_ = this->create_publisher<sensor_msgs::msg::MagneticField>("~/magnetic_field", 10);
        pub_delta_q_ = this->create_publisher<geometry_msgs::msg::QuaternionStamped>("~/delta_q", 10);
        pub_delta_v_ = this->create_publisher<geometry_msgs::msg::Vector3Stamped>("~/delta_v", 10);

        imu_msg_.header.frame_id = frame_id;
        mag_msg_.header.frame_id = frame_id;
        delta_q_msg_.header.frame_id = frame_id;
        delta_v_msg_.header.frame_id = frame_id;

        // Use periodic timer to trig activity
        timer_ = this->create_wall_timer(1ms, std::bind(&XsensMtiNode::update_callback, this));

        RCLCPP_INFO(this->get_logger(), "Node started");
    }

  private:
    void update_callback()
    {
        xsens::frame_t frame;

        if (serial_.available_byte()<16)
            return;

        if (serial_.read_frame(frame) && (frame.m_id == 0x36))
        {
            auto ts = this->now();
            xsens::data_t data;
            xsens::parse_frame(frame.data, frame.len, data);

            bool imu_data_flag = false; // check if at least one of the submessage is available to publish imu_data

            if (data.orientation_flag)
            {
                imu_msg_.orientation.w = data.orientation.w;
                imu_msg_.orientation.x = data.orientation.x;
                imu_msg_.orientation.y = data.orientation.y;
                imu_msg_.orientation.z = data.orientation.z;

                this->get_parameter("covariance/orientation/roll", imu_msg_.orientation_covariance[0]);
                this->get_parameter("covariance/orientation/pitch", imu_msg_.orientation_covariance[4]);
                this->get_parameter("covariance/orientation/yaw", imu_msg_.orientation_covariance[8]);

                imu_data_flag = true;
            }

            if (data.angular_velocity_flag)
            {
                imu_msg_.angular_velocity.x = data.angular_velocity.x;
                imu_msg_.angular_velocity.y = data.angular_velocity.y;
                imu_msg_.angular_velocity.z = data.angular_velocity.z;

                this->get_parameter("covariance/angular_velocity/x", imu_msg_.angular_velocity_covariance[0]);
                this->get_parameter("covariance/angular_velocity/y", imu_msg_.angular_velocity_covariance[4]);
                this->get_parameter("covariance/angular_velocity/z", imu_msg_.angular_velocity_covariance[8]);
                imu_data_flag = true;
            }

            if (data.linear_acceleration_flag)
            {
                imu_msg_.linear_acceleration.x = data.linear_acceleration.x;
                imu_msg_.linear_acceleration.y = data.linear_acceleration.y;
                imu_msg_.linear_acceleration.z = data.linear_acceleration.z;

                this->get_parameter("covariance/linear_acceleration/x", imu_msg_.linear_acceleration_covariance[0]);
                this->get_parameter("covariance/linear_acceleration/y", imu_msg_.linear_acceleration_covariance[4]);
                this->get_parameter("covariance/linear_acceleration/z", imu_msg_.linear_acceleration_covariance[8]);
                imu_data_flag = true;
            }

            if(imu_data_flag)
            {
                imu_msg_.header.stamp = ts;
                pub_imu_->publish(imu_msg_);
            }

            if (data.magnetic_field_flag)
            {
                mag_msg_.header.stamp = ts;
                mag_msg_.magnetic_field.x = data.magnetic_field.x;
                mag_msg_.magnetic_field.y = data.magnetic_field.y;
                mag_msg_.magnetic_field.z = data.magnetic_field.z;
                pub_mag_->publish(mag_msg_);
            }

            if (data.delta_q_flag)
            {
                delta_q_msg_.header.stamp = ts;
                delta_q_msg_.quaternion.w = data.delta_q.w;
                delta_q_msg_.quaternion.x = data.delta_q.x;
                delta_q_msg_.quaternion.y = data.delta_q.y;
                delta_q_msg_.quaternion.z = data.delta_q.z;
                pub_delta_q_->publish(delta_q_msg_);
            }

            if (data.delta_v_flag)
            {
                delta_v_msg_.header.stamp = ts;
                delta_v_msg_.vector.x = data.delta_v.x;
                delta_v_msg_.vector.y = data.delta_v.y;
                delta_v_msg_.vector.z = data.delta_v.z;
                pub_delta_v_->publish(delta_v_msg_);
            }
        }

    }

    xsens::Serial serial_;
    rclcpp::Publisher<sensor_msgs::msg::Imu>::SharedPtr pub_imu_;
    rclcpp::Publisher<sensor_msgs::msg::MagneticField>::SharedPtr pub_mag_;
    rclcpp::Publisher<geometry_msgs::msg::QuaternionStamped>::SharedPtr pub_delta_q_;
    rclcpp::Publisher<geometry_msgs::msg::Vector3Stamped>::SharedPtr pub_delta_v_;
    rclcpp::TimerBase::SharedPtr timer_;
    sensor_msgs::msg::Imu imu_msg_;
    sensor_msgs::msg::MagneticField mag_msg_;
    geometry_msgs::msg::QuaternionStamped delta_q_msg_;
    geometry_msgs::msg::Vector3Stamped delta_v_msg_;
};

//-----------------------------------------------------------------------------
int main(int argc, char **argv)
{
    rclcpp::init(argc, argv);

    try
    {
        auto node = std::make_shared<XsensMtiNode>();
        rclcpp::spin(node);
    }
    catch (std::string &e)
    {
        std::cout << "Error: " << e << std::endl;
    }

    rclcpp::shutdown();
    return 0;
}