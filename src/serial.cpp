#include <string>
#include <unistd.h>
#include <termios.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <linux/serial.h>
#include <csignal>
#include <thread>
#include <chrono>
#include <vector>
#include <iostream>
#include "xsens/serial.hpp"

using namespace std::literals::chrono_literals;
using namespace xsens;

//------------------------------------------------------------------------------
void Serial::open_device(std::string device, int baudrate, int timeout_ms)
{
    // Check baudrate
    speed_t b;
    switch(baudrate)
    {
    case 9600:   b = B9600;break;
    case 19200:  b = B19200;break;
    case 38400:  b = B38400;break;
    case 57600:  b = B57600;break;
    case 115200: b = B115200;break;
    case 230400: b = B230400;break;
    case 460800: b = B460800;break;
    case 921600: b = B921600;break;
    default:
        throw std::string("Cannot open serial port: incompatible baudrate");
    }

    // Open chardev
    fd_ = open(device.c_str(), O_RDWR | O_NDELAY);
    if(fd_ < 0)
        throw std::string("Cannot open serial port: error while openning ")+device;

    // Configure device
    struct termios term;
    tcgetattr(fd_, &term);

    // Store previous configuration
    tcgetattr(fd_, &old_term_);

    // RAW mode, non canonical, no echo
    term.c_iflag = IGNBRK;
    term.c_lflag = 0;
    term.c_oflag = 0;

    term.c_cflag |= (CREAD | CLOCAL);       //
    term.c_cflag &= ~(PARENB | CSIZE);      // No parity
    term.c_cflag |= CS8;                    // Data 8 bits
    term.c_iflag &= ~(IXON|IXOFF|IXANY);    // No XON/XOFF flow control

    if(cfsetospeed(&term, b)<0)
        throw std::string("Cannot set serial port baudrate to ")+std::to_string(b);

    if(cfsetispeed(&term, b)<0)
        throw std::string("Cannot set serial port baudrate to ")+std::to_string(b);

    tcsetattr(fd_, TCSANOW, &term);

    // low level configuration
    struct serial_struct serinfo;
    ioctl (fd_, TIOCGSERIAL, &serinfo);
    serinfo.flags |= ASYNC_LOW_LATENCY;
    ioctl (fd_, TIOCSSERIAL, &serinfo);

    timeout_us_ = int(timeout_ms*1000);
}

//------------------------------------------------------------------------------
void Serial::close_device()
{
    if (fd_ > 0)
    {
        tcsetattr(fd_, TCSANOW, &old_term_);
        close(fd_);
    }
}

int Serial::available_byte()
{
    int bytes;
    ioctl(fd_, FIONREAD, &bytes);
    return bytes;
}

//-----------------------------------------------------------------------------
void Serial::read_byte(uint8_t *data, int size)
{
    int count = 0;
    int n = 0;
    while (true)
    {
        ioctl(fd_, FIONREAD, &n);
        if (n >= size)
            break;
        std::this_thread::sleep_for(100us);

        // check for timeout
        count += 100;
        if (count > timeout_us_)
            throw std::string("Timeout when reading data on serial port: ")+std::to_string(count)+std::string(" us");
    }
    read(fd_, data, size);
}

//------------------------------------------------------------------------------
void Serial::go_to_measurement()
{
    uint8_t data[]= { 0xFA, 0xFF, 0x10, 0x00, 0xF1};
    write(fd_, data, sizeof(data));
}

//------------------------------------------------------------------------------
bool Serial::read_frame(frame_t &frame)
{
    // Start of frame
    bool header_find=false;
    while(!header_find)
    {
        bool preamble=false;
        while(!preamble)
        {
            uint8_t byte;
            read_byte(&byte, 1);
            preamble = (byte==0xFA);
        }

        uint8_t byte;
        read_byte(&byte,1);
        header_find = (byte==0xFF);
    }

    // Checksum
    uint8_t cks = 0xFF;

    // Message ID + Len
    read_byte((uint8_t *)&frame.m_id, 1);
    cks += frame.m_id;
    read_byte((uint8_t *)&frame.len, 1);
    cks += frame.len;

    // Data length
    int length;
    if (frame.len!=0xFF)
    {
        length = frame.len;
        frame.ext_len = 0;
    }
    else
    {
        read_byte((uint8_t *)&frame.ext_len, 2);
        length = frame.ext_len;
        cks += frame.ext_len&0xFF;
        cks += frame.ext_len>>8;
    }

    // Data
    if (0 < length && length < 2048)
    {
        read_byte((uint8_t*)&frame.data, length);
        for (int i=0; i<length; i++)
            cks += frame.data[i];
    }
    else
    {
        return false;
    }

    // Chksum
    read_byte((uint8_t*)&frame.chksum, 1);
    cks += frame.chksum;
    return (cks==0x00);
}

