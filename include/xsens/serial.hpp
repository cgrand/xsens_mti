/*
 * Copyright 2021 ONERA
 *
 * This file is part of the Robotis project.
 *
 * Robotis is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * Robotis is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MAUVE.  If not, see <https://www.gnu.org/licenses/gpl-3.0>.
 */
#ifndef XSENS_MTI__HPP
#define XSENS_MTI__HPP

#include <string>
#include <vector>
#include <termios.h>

namespace xsens
{

#pragma pack(1)
typedef struct
{
    uint8_t  m_id;
    uint8_t  len;
    uint16_t ext_len;
    uint8_t  data[2048];
    uint8_t  chksum;
} frame_t;
#pragma pack()


class Serial
{
  public:
    Serial(): fd_(-1){};
    ~Serial()
    {
        close_device();
    }

    /// Open serial port for sensor communication
    void open_device(std::string device, int baudrate, int timeout_ms);
    /// Close serial port
    void close_device();
    /// Set read frame timeout in millisecond
    void set_timeout_ms(int timeout) { timeout_us_ = timeout * 1000; }
    /// Get read frame timeout in millisecond
    int get_timeout_ms() { return timeout_us_ / 1000; }
    /// Read xsens mti frame
    bool read_frame(frame_t &frame);
    /// Send go_to_measurement frame
    void go_to_measurement();
    /// Get number of byte available
    int available_byte();

  protected:
    /// Serial port read timeout in ms
    int timeout_us_;
    /// Serial port file descriptor
    int fd_;
    /// Use to store previous serial port configuration
    struct termios old_term_;
    /// Read byte with time
    void read_byte(uint8_t *data, int size);
};

} // namespace xsens_mti
#endif
