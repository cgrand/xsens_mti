#ifndef XSENS_MTI__PROTOCOL__HPP
#define XSENS_MTI__PROTOCOL__HPP

#include <string>
#include <sstream>

namespace xsens
{
#pragma pack(1)

/// Struct Quaternion
typedef struct
{
    float w;
    float x;
    float y;
    float z;
} quaternion_t;

/// Struct Vector3
typedef struct
{
    float x;
    float y;
    float z;
} vector3_t;

/// Union: Status Byte and byte field
typedef union {
    struct
    {
        uint8_t
        self_test : 1,
        filter_valid : 1,
        gnss_fix : 1,
        no_rotation_update : 2,
        representative_motion : 1,
        reserved : 2;
    };
    uint8_t byte;
} status_t;

/// Struct xsens mtdata2
typedef struct
{
    uint64_t timestamp_ns;
    uint16_t packet_counter;    //
    bool packet_counter_flag;
    uint32_t sample_time;       // scale 0.1 ms
    bool sample_time_flag;
    uint8_t status_byte;
    quaternion_t orientation;
    bool orientation_flag;
    vector3_t linear_acceleration;
    bool linear_acceleration_flag;
    vector3_t angular_velocity;
    bool angular_velocity_flag;
    vector3_t magnetic_field;
    bool magnetic_field_flag;
    quaternion_t delta_q;
    bool delta_q_flag;
    vector3_t delta_v;
    bool delta_v_flag;
} data_t;

#pragma pack()

/// Enum: precision format
typedef enum : uint16_t
{
    Float32 = 0x0000,
    Fp1220 = 0x0001,
    Fp1632 = 0x0002,
    Float64 = 0x0003
} precision_t;

// Enum: coordinate System
typedef enum : uint16_t
{
    ENU = 0x0000,
    NED = 0x0004,
    NWU = 0x0008
} coordinates_t;

namespace xdi
{
static const uint16_t PacketCounter = 0x1020;
static const uint16_t SampleTimeFine = 0x1060;
static const uint16_t Temperature = 0x0810;
static const uint16_t BaroPressure = 0x3010; // Pascal
static const uint16_t Quaternion = 0x2010;
static const uint16_t DeltaV = 0x4010;           // m/s
static const uint16_t Acceleration = 0x4020;     // m/s²
static const uint16_t RateOfTurn = 0x8020;       // rad/s
static const uint16_t DeltaQ = 0x8030;           // -
static const uint16_t RawAccGyrMagTemp = 0xA010; //
static const uint16_t MagneticField = 0xC020;    // a.u.
static const uint16_t StatusByte = 0xE010;
static const uint16_t StatusWord = 0xE020;

inline std::string to_string(uint16_t id)
{
    switch(id)
    {
      case PacketCounter:  return "PacketCounter";
      case SampleTimeFine: return "SampleTimeFine";
      case Temperature:    return "Temperature";
      case BaroPressure:   return "BaroPressure";
      case Quaternion:     return "Quaternion";
      case Acceleration:   return "Acceleration";
      case RateOfTurn:     return "RateOfTurn";
      case MagneticField:  return "MagneticField";
      case StatusByte:     return "StatusByte";
      case StatusWord:     return "StatusWord";
      default:             return "Unknown";
    }

    std::ostringstream ss("0x");
    ss<<std::hex<<id<<std::dec;
    return ss.str();
}
} // namespace xdi

void parse_frame(const uint8_t *buffer, int len, data_t &data);

} // namespace xsens

std::ostream& operator<<(std::ostream& o, const xsens::data_t& data );

#endif
